#!/bin/bash
#####################################################################################
# author: Jantinus Daling
# license: GPLv3

# John 3:16
# “For God so loved the world, that he gave his only begotten Son, 
# that whosoever believeth in him should not perish, but have everlasting life.”
#####################################################################################

sudo apt update &&
sudo apt upgrade -y &&
sudo apt install python3 python3-pip ufw git -y &&
sudo git clone https://gitlab.com/jtdaling/openirrigation.git /opt/openirrigation &&
sudo chown $USER /opt/openirrigation &&
sudo pip3 -r /opt/openirrigation/requirements.txt &&

sudo chmod 755 -R /opt/openirrigation/ &&


sudo echo -n "[Unit]
Description=Open Irrigation Scheduler
After=syslog.target

[Service]
Type=simple
WorkingDirectory=/opt/openirrigation
ExecStart=python3 /opt/openirrigation/irrigation.py
StandardOutput=syslog
StandardError=syslog

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/openirrigation.service &&


sudo echo -n "[Unit]
Description=Weather data downloader
After=syslog.target

[Service]
Type=simple
WorkingDirectory=/opt/openirrigation
ExecStart=python3 /opt/openirrigation/weatherdata.py
StandardOutput=syslog
StandardError=syslog

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/weatherdata.service &&


sudo echo -n "[Unit]
Description=Open Irrigation web interface
After=syslog.target

[Service]
Type=simple
User=root
WorkingDirectory=/opt/openirrigation
ExecStart=python3 /opt/openirrigation/main.py
StandardOutput=syslog
StandardError=syslog

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/irrigationinterface.service &&

sudo systemctl daemon-reload &&
sudo systemctl start openirrigation.service &&
sudo systemctl enable openirrigation.service &&
sudo systemctl start weatherdata.service &&
sudo systemctl enable weatherdata.service &&
sudo systemctl start irrigationinterface.service &&
sudo systemctl enable irrigationinterface.service &&

git clone https://github.com/WiringPi/WiringPi.git /tmp/WiringPi/&&
cd /tmp/WiringPi &&
git pull origin &&
./build &&
cd .. &&
rm -rf /tmp/WiringPi &&

sudo sed -i "/ENABLED=/d" /etc/ufw/ufw.conf &&
sudo sed -i "6 i ENABLED=yes" /etc/ufw/ufw.conf &&
sudo ufw allow 22/tcp &&
sudo ufw allow 80 &&
sudo systemctl enable ufw &&
sudo ufw reload 
